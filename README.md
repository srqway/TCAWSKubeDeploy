# TechCrumble Kubernetes Cluster Deployment With Terraform And Ansible

This project is created to deploy a three node Kubernetes cluster on AWS for my testing purposes. Cluster is automatically deployed using GitLab CI/CD Pipelines and installing necessary libraries to run the Kubernetes cluster

All together cluster Contains with four Nodes:
*  Kube Master
*  3 x Kube workers

This deployment contains with four main stages:
*  Validate
*  Plan
*  Apply
*  Deploy

Set of Ansible Playbooks will ran after the Terraform Deployment. Ansible Playbooks will perform below tasks
* Install Dependencies in Master and Worker nodes
* Install the Flannel Network
* Setup Kubernetes Master and initialize the cluster
* Connect Worker Nodes to the Master

Version 1 release is setup as the tested and confirmed version.